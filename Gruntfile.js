var grunt = require('grunt');
grunt.loadNpmTasks('grunt-aws-lambda');

grunt.initConfig({
	lambda_invoke: {
		recieve_call: {
			options: {
				handler: 'recieveCall',
				file_name: 'index.js',
				event: 'event/event.recieve_call.json'
			}
		},
		make_call: {
			options: {
				handler: 'makeCall',
				file_name: 'index.js',
				event: 'event/event.make_call.json'
			}
		},
		send_sms: {
			options: {
				handler: 'sendSms',
				file_name: 'index.js',
				event: 'event/event.send_sms.json'
			}
		},
		recieve_sms: {
			options: {
				handler: 'recieveSms',
				file_name: 'index.js',
				event: 'event/event.recieve_sms.json'
			}
		},
		recieve_mms: {
			options: {
				handler: 'recieveMms',
				file_name: 'index.js',
				event: 'event/event.recieve_mms.json'
			}
		}
	},
	lambda_package: {
		default: {
		}
	}
});