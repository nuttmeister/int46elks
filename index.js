// Setup the provider object with necessary auth (46elks.com)
var provider = require('./lib/pbx/46elks/provider.js');
provider.init(null,'u9f5e223104f03c7ed529149fb487332d','6190074D38548A9CEE9145F7E8E6035B');

var recieveCall = require('./lib/pbx/recievecall.js');
// var makeCall = require('./lib/pbx/makecall.js');
// var sendSms = require('./lib/pbx/sendsms.js');
// var recieveSms = require('./lib/pbx/recievesms.js');
// var recieveMms = require('./lib/pbx/recievemms.js');

// For recieving calls - responds with a JSON tree containing call logic (response will be an internal Object)
exports.recieveCall = function (event, context, callback) {
	// Var contains allowed IP-adresses
	var ipAddr = event.ipaddr;
	var allowedIps = [ 'test-invoke-source-ip', '185.32.8.178', '62.109.57.12', '212.112.190.140', '176.10.154.199', '2001:9b0:2:902::199' ];
	if (allowedIps.indexOf(ipAddr) === -1) return '';

	// Check if we have the correct key
	var key = event.key;
	var allowedKey = 'c9c3GJDDxYwynaFwHfvkHL4V';
	if (key !== allowedKey) return '';

	var direction = event.direction;
	var from = event.from;
	var to = event.to;
	var callid = event.callid;

	recieveCall(from, to, callid, direction, function(response) {
		callback(null, response.data());
	});
}

/*// For making outgoing calls. 
exports.makeCall = function (event, context, callback) {
	var from = event.from;
	var to = event.to;
	var voice_start = event.voice_start;
	var whenhangup = event.whenhangup;

	makeCall(from, to, voice_start, whenhangup, function(response) {
		callback(null, response);
	});
}*/

/*// For sending SMS - makes an API call to the provider for sending SMS (response will be an JS Object)
exports.sendSms = function (event, context, callback) {
	var from = event.from;
	var to = event.number;
	var message = event.message;
	var flash = (event.flash === 'yes') ? 'yes' : null;

	sendSms(from, to, message, flash, function(response) {
		callback(null, response);
	});
}*/

/*// For recieving SMS and optionally sending a reply back through callback, note that the reply should be text/plain
exports.recieveSms = function (event, context, callback) {
	var from = event.from;
	var to = event.to;
	var message = event.message;

	recieveSms(from, to, message, function(response) {
		callback(null, response);
	});
}*/

/*// For recieving MMS
exports.recieveMms = function (event, context, callback) {
	var from = event.from;
	var to = event.to;
	var message = event.message;
	var image = event.image;

	recieveMms(from, to, message, image, function(response) {
		callback(null, response);
	});
}*/