// Function to check if an Object is empty

module.exports = function(obj) {
	return !Object.keys(obj).length;
}