// Function to return the current datetime

module.exports = function() {
	var datetime = new Date();
	return datetime.toISOString();
}