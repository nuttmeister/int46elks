// Victorops integration - Grab the respective team on call number.
var https = require('https');

// Define URL/API constants 
const VO_HOST = 'portal.victorops.com';
const VO_PORT = 443;
const VO_BASE_PATH = '/api/v1/org/';
const VO_TEAMS_PATH = '/teams';
const VO_USER_BASE_PATH = '/profile/';
const VO_USER_END_PATH = '/methods';

// Auth
var apiusername = null;
var apipassword = null;
var apiorg = null;

// Private vars
var teams = {}

// Grab data from the VO Portal API via HTTPS. Unlike the public API this has no limits on calls per minute.
var getData = function(cUrl, cb, cOptional) {
	var options = {
		host: VO_HOST,
		port: VO_PORT,
	    path: VO_BASE_PATH + cUrl,
		headers: { 
			'Accept': 'application/json',
			'Authorization': 'Basic ' + new Buffer(apiusername + ':' + apipassword).toString('base64')
		}
 	}
	https.get(options, function(res) {
		var data = '';
		res.on('data', function(chunk) {
			data += chunk;
		});
		res.on('end', function() {
			cb(data, cOptional);
        });
	}).on('error', function(err) {
		cb(null, null)
	});
}

// Init the object, all 3 are required for VO to work (string|string|string)
exports.init = function(cApiUsername, cApiPassword, cApiOrg) {
	apiusername = typeof cApiUsername !== 'undefined' ? cApiUsername : apiusername;
	apipassword = typeof cApiPassword !== 'undefined' ? cApiPassword : apipassword;
	apiorg = typeof cApiOrg !== 'undefined' ? cApiOrg : apiorg;
}

// Grab the oncall number for all the teams
exports.getOnCallList = function(cb) {
	// Retrieve all the teams
	getData(apiorg + VO_TEAMS_PATH, function(cTeams) {
		var teamsParse = JSON.parse(cTeams);
		for (var index in teamsParse) {
			var teamIndex = teamsParse[index];
			var slug = typeof teamIndex.slug !== 'undefined' ? teamIndex.slug : null;
			if (typeof teamIndex.oncall !== 'undefined') {
				var oncall = typeof teamIndex.oncall[0] !== 'undefined' ? teamIndex.oncall[0]['oncall'] : null;
			}
			if ((slug !== null) && (oncall !== null)) {
				teams[slug] = oncall;
			}
		}

		// Get the specific Phone number for the specific team and format it correctly
		for (var index in teams) {
			var count = 0;
			var teamLength = Object.keys(teams).length;
			getData(apiorg + VO_USER_BASE_PATH + teams[index] + VO_USER_END_PATH, function(cUser, cTeam) {
				var userParse = JSON.parse(cUser);
				for (var index in userParse) {
					count++;
					if (userParse[index]['type'] === 'Phone') {
						teams[cTeam] = userParse[index]['value'].replace(/ /g,'');
						break;
					}
				}
				// Return the specific key and number to the callback.
				// Make sure we only call the callback once and that last time when teams object is complete.
				if (teamLength === count) {
					cb(teams);
				}
			}, index);
		}

		// If teams are empty, send back empty string
		if (Object.keys(teams).length === 0) {
			cb(teams);
		}

	});
}