var provider = require('./46elks/provider.js');
var sms = require('./46elks/sms.js');

module.exports = function(cFrom, cTo, cMessage, cFlashSms, cb) {
	// Create the SMS object
	var sms1 = new sms;
	sms1.init(cFrom, cTo, cMessage, cFlashSms);

	// Send the SMS object with the selected provider
	provider.sendSms(sms1, cb);
}