// Module for setting up 46elks.com as a provider.

// API URI's used by 46elks
const ELKS_HOST = 'api.46elks.com';
const ELKS_PORT = 443;
const ELKS_SMS_URI = '/a1/SMS';
const ELKS_CALL_URI = '/a1/Calls';
const ELKS_METHOD = 'POST';

// Auth
var apitoken = null;
var apiusername = null;
var apipassword = null;

// Private functions
// Send HTTP POST to 46elks.com API
var sendData = function(cPath, cData, cb) {
	// We will not always need to send stuff with the provider, hence the require not on top
	var https = require('https');
	var querystring = require('querystring');

	var qs = querystring.stringify(cData.data());

	var options = {
		host: ELKS_HOST,
		port: ELKS_PORT,
	    path: cPath,
	    method: ELKS_METHOD,
		headers: { 
			'Accept': 'application/json',
			'Authorization': 'Basic ' + Buffer(apiusername + ':' + apipassword).toString('base64'),
			'Content-Type': 'application/x-www-form-urlencoded',
        	'Content-Length': Buffer.byteLength(qs)
		}
 	}
	var req = https.request(options, function(res) {
		var data = '';
		res.on('data', function(chunk) {
			data += chunk;
		});
		res.on('end', function() {
			cb(JSON.parse(data));
        });
	});
	req.write(qs);
	req.on('error', function(err) {
		console.error('Error: ' + err);
		console.error(cData);
	});
	req.end();
}

// Functions
exports.init = function(cApiToken, cApiUsername, cApiPassword) {
	apitoken = typeof cApiToken !== 'undefined' ? cApiToken : apitoken;
	apiusername = typeof cApiUsername !== 'undefined' ? cApiUsername : apiusername;
	apipassword = typeof cApiPassword !== 'undefined' ? cApiPassword : apipassword;
}

exports.sendSms = function(cSms, cb) {
	// Send to server
	sendData(ELKS_SMS_URI, cSms, cb);
}

exports.makeCall = function(cCall, cb) {
	// Send to server
	sendData(ELKS_CALL_URI, cCall, cb)
}