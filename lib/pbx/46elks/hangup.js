// Module for 46elks - hangup

module.exports = function () {
	// Private variables.
	var changup = 'reject'; // Hangup cause (string) (busy|reject|404)

    // Exposed functions and variables. 
    return {
    	init: function(cHangup) {
    		hangup = typeof cHangup !== 'undefined' ? cHangup : hangup;
    	},
        data: function() {
            return (hangup !== null) ? { hangup: hangup } : { hangup: 'reject' };
        },
        json: function() {
            return JSON.stringify({ hangup: hangup });
        }
	};
};