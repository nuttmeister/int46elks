// Module for 46elks - play
var merge = require('../../merge.js');

module.exports = function () {
	// Private variables.
	var play = ''; // The file to play (string).
	var failed = null; // Action to do if we fail to play the file (string).
    var next = null; // Continue here, regardless. If URL the result will be included. (string)

    // Combine values
    var combine = function () {
        var data = (play !== null) ? merge(data, { play: play }) : {};
        data = (failed !== null) ? merge(data, { failed: failed }) : data;
        data = (next !== null) ? merge(data, { next: next }) : data;
        return data;
    }

    // Exposed functions and variables. 
    return {
    	init: function(cPlay, cFailed, cNext) {
    		play = typeof cPlay !== 'undefined' ? cPlay : play;
    		failed = typeof cFailed !== 'undefined' ? cFailed : failed;
    		next = typeof cNext !== 'undefined' ? cNext : next;
    	},
        data: function() {
            return combine();
        },
        json: function() {
            return JSON.stringify(combine());
        }
	};
};