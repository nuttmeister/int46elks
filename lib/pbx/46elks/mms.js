// Module for 46elks - MMS
var merge = require('../../merge.js');

module.exports = function () {
	// Private variables.
	var from = null; // The from number (alphanumeric) (string)
	var to = null; // The to number (numeric) (string)
    var message = null; // The message to send as sms (string)
    var image = null; // URL to the Image in the MMS (string)

    // Combine values
    var combine = function () {
        var data = (from !== null) ? { from: from } : {};
        data = (to !== null) ? merge(data, { to: to }) : data;
        data = (message !== null) ? merge(data, { message: encodeURI(message) }) : data;
        data = (image !== null) ? merge(data, { image: image }) : data;
        return data;
    }

    // Exposed functions and variables. 
    return {
        // Init the object
    	init: function(cFrom, cTo, cMessage, cImage) {
    		from = typeof cFrom !== 'undefined' ? cFrom : from;
    		to = typeof cTo !== 'undefined' ? cTo : to;
    		message = typeof cMessage !== 'undefined' ? cMessage : message;
            image = typeof cImage !== 'undefined' ? cImage : image;
    	},
        // Return the objects data
        data: function() {
            return combine();
        },
        // Return the objects data as JSON
        json: function() {
            return JSON.stringify(combine());
        }
	};
};