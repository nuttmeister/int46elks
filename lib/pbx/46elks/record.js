// Module for 46elks - record
var merge = require('../../merge.js');

module.exports = function () {
	// Private variables.
	var record = ''; // The URL to send the recording to (string).
	var timelimit = null; // Change the default timelimit of the recording (integer)

    // Combine values
    var combine = function () {
        var data = (record !== null) ? merge(data, { record: record }) : {};
        data = (timelimit !== null) ? merge(data, { timelimit: timelimit }) : data;
        return data;
    }

    // Exposed functions and variables. 
    return {
    	init: function(cRecord, cTimelimit) {
    		record = typeof cRecord !== 'undefined' ? cRecord : record;
    		timelimit = typeof cTimelimit !== 'undefined' ? cTimelimit : timelimit;
    	},
        data: function() {
            return combine();
        },
        json: function() {
            return JSON.stringify(combine());
        }
	};
};