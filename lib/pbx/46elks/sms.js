// Module for 46elks - SMS
var merge = require('../../merge.js');

module.exports = function () {
	// Private variables.
	var from = null; // The from number (alphanumeric) (string)
	var to = null; // The to number (numeric) (string)
    var message = null; // The message to send as sms (string)
    var flashsms = null; // If we should send the SMS as a Flash SMS (string)

    // Combine values
    var combine = function () {
        var data = (from !== null) ? { from: from } : {};
        data = (to !== null) ? merge(data, { to: to }) : data;
        data = (message !== null) ? merge(data, { message: message }) : data;
        data = (flashsms !== null) ? merge(data, { flashsms: flashsms }) : data;
        return data;
    }

    // Exposed functions and variables. 
    return {
        // Init the object (not necessary when replying)
    	init: function(cFrom, cTo, cMessage, cFlashSms) {
    		from = typeof cFrom !== 'undefined' ? cFrom : from;
    		to = typeof cTo !== 'undefined' ? cTo : to;
    		message = typeof cMessage !== 'undefined' ? cMessage : message;
            flashsms = typeof cFlashSms !== 'undefined' ? cFlashSms : flashsms;
    	},
        // Directly reply to an incomming SMS
        reply: function(cMessage) {
            return typeof cMessage !== 'undefined' ? cMessage : '';
        },
        // Return the objects data
        data: function() {
            return combine();
        },
        // Return the objects data as JSON
        json: function() {
            return JSON.stringify(combine());
        }
	};
};