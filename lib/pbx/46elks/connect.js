// Module for 46elks - connect
var merge = require('../../merge.js');

module.exports = function () {
	// Private variables.
	var connect = ''; // The number to dial (string)
	var callerid = null; // The callerid to show if empty we will show the callers callerid (string)
	var busy = null; // Busy, executed then we recieve busy tone. (string)
    var success = null; // Answered, executed when the remote party has hung up. (string)
    var failed = null; // No Answer och the phone couldn't be reached. (string)
    var recordcall = null; // If specified records the call and sends it to the URL specified (string)
    var next = null; // Continue here, regardless. If URL the result will be included. (string)

    // Combine values
    var combine = function () {
        var data = (connect !== null) ? { connect: connect } : {};
        data = (callerid !== null) ? merge(data, { callerid: callerid }) : data;
        data = (busy !== null) ? merge(data, { busy: busy }) : data;
        data = (success !== null) ? merge(data, { success: success }) : data;
        data = (failed !== null) ? merge(data, { failed: failed }) : data;
        data = (recordcall !== null) ? merge(data, { recordcall: recordcall }) : data;
        data = (next !== null) ? merge(data, { next: next }) : data;
        return data;
    }

    // Exposed functions and variables. 
    return {
    	init: function(cConnect, cCallerId, cBusy, cSuccess, cFailed, cRecordCall, cNext) {
    		connect = typeof cConnect !== 'undefined' ? cConnect : connect;
    		callerid = typeof cCallerId !== 'undefined' ? cCallerId : callerid;
    		busy = typeof cBusy !== 'undefined' ? cBusy : busy;
    		success = typeof cSuccess !== 'undefined' ? cSuccess : success;
            failed = typeof cFailed !== 'undefined' ? cFailed : failed;
            recordcall = typeof cRecordCall !== 'undefined' ? cRecordCall : failed;
    		next = typeof cNext !== 'undefined' ? cNext : next;
    	},
        data: function() {
            return combine();
        },
        json: function() {
            return JSON.stringify(combine());
        }
	};
};