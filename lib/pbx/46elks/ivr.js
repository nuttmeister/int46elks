// Module for 46elks - ivr
var merge = require('../../merge.js');
var isEmpty = require('../../isempty.js');

module.exports = function () {
	// Private variables and functions.
	var play = ''; // The main menu files. (string)
	var destinations = {}; // Add destinations. (integer, string)
	var timeout = null; // Timeout to wait after main menu file has ended playing. (integer)
	var repeat = null; // Number of retries (plays main menu) before we should hangup. (integer)
	var digits = null; // If we want to input more than 1 char, please use next (with URL), destinations are not avaible in this case. (integer)
	var next = null; // Continue here regardsless or used as URL for digits. (string)

	// Combine values
	var combine = function () {
        var data = (play !== null) ? merge(data, { ivr: play }) : {};
    	data = (!isEmpty(destinations)) ? merge(data, destinations) : data;
    	data = (timeout !== null) ? merge(data, { timeout: timeout }) : data;
    	data = (repeat !== null) ? merge(data, { repeat: repeat }) : data;
    	data = (digits !== null) && (isEmpty(destinations)) ? merge(data, { digits: digits }) : data;
    	data = (next !== null) ? merge(data, { next: next }) : data;
    	return data;
	}

	// Exposed functions and variables.	
    return {
    	init: function(cPlay, cTimeout, cRepeat, cDigits, cNext) {
    		play = typeof cPlay !== 'undefined' ? cPlay : play;
    		timeout = typeof cTimeout !== 'undefined' ? cTimeout : timeout;
    		repeat = typeof cRepeat !== 'undefined' ? cRepeat : repeat;
    		digits = typeof cDigits !== 'undefined' ? cDigits : digits;
    		next = typeof cNext !== 'undefined' ? cNext : next;
    	},
    	addDestination: function(cNumber, cDestination) {
    		destinations[cNumber] = cDestination;
    	},
    	data: function() {
    		return combine();
    	},
    	json: function() {
    		return JSON.stringify(combine());
    	}
	};
};