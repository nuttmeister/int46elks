// Module for 46elks - Call
var merge = require('../../merge.js');

module.exports = function () {
	// Private variables.
	var from = null; // The from number (alphanumeric) (string)
	var to = null; // The to number (numeric) (string)
    var voice_start = null; // URL or JSON of actions to execute when call is answered.
    var hangup = null; // URL to post to when call hangup or failed.

    // Combine values
    var combine = function () {
        var data = (from !== null) ? { from: from } : {};
        data = (to !== null) ? merge(data, { to: to }) : data;
        data = (voice_start !== null) ? merge(data, { voice_start: voice_start }) : data;
        data = (hangup !== null) ? merge(data, { hangup: hangup }) : data;
        return data;
    }

    // Exposed functions and variables. 
    return {
        // Init the object (not necessary when replying)
    	init: function(cFrom, cTo, cVoiceStart, cHangup) {
    		from = typeof cFrom !== 'undefined' ? cFrom : from;
    		to = typeof cTo !== 'undefined' ? cTo : to;
    		voice_start = typeof cVoiceStart !== 'undefined' ? cVoiceStart : voice_start;
            hangup = typeof cHangup !== 'undefined' ? cHangup : hangup;
    	},
        // Return the objects data
        data: function() {
            return combine();
        },
        // Return the objects data as JSON
        json: function() {
            return JSON.stringify(combine());
        }
	};
};