var provider = require('./46elks/provider.js');
var mms = require('./46elks/mms.js');

module.exports = function(cFrom, cTo, cMessage, cImage, cb) {
	// Create the MMS object - not always necessary, but looks nice
	var mms1 = new mms;
	mms1.init(cFrom, cTo, cMessage, cImage);

	// Do something with the MMS (store in DB, etc), don't return anything to callback (any return value/text will be ignored by the API)

}