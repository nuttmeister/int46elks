var provider = require('./46elks/provider.js');
var vo = require('../source_providers/victorops.js');
var ivr = require('./46elks/ivr.js');
var connect = require('./46elks/connect.js');
var play = require('./46elks/play.js');

module.exports = function (cFrom, cTo, cCallId, cDirection, cb) {
	var teams = {};
	// Setup the Victorops integration
	vo.init('dwtech', '9Bwy3#r4H06thxsW5', 'daniel-wellington-technologies-ab');
	vo.getOnCallList(function(cTeams) {
		teams = cTeams;
		executePbx();
	});

	var executePbx = function() {
		// Setup the CallMap for the teams and different choices
		var destinationMap = {
			1: 'itops',
			2: 'ecom-daniel-wellington',
			3: 'ecom-nicole-vienna'
		}

		// Create object for numbres
		var number = {}

		// Set a backup number to dial if the API doesnt return a oncall value
		var backupNumber = {
			1: '+460733305970',
			2: '+460733305970',
			3: '+460733305970'
		}

		// Setup the numbers - If the number is not correct or has not been setup we replace it with the backup number
		for (var index in destinationMap) {
			teams[destinationMap[index]] = (typeof teams[destinationMap[index]] !== 'undefined') ? teams[destinationMap[index]] : backupNumber[index];
			number[index] = (teams[destinationMap[index]].charAt(0) === '+') ? teams[destinationMap[index]] : backupNumber[index];
		}

		// CallerID to show to outgoing calls
		// Usually we should use cFrom var, but we want to show a static number
		var callerid = '+46766862154';

		// IVR-menu file to playback on incoming calls
		var ivrfile = 'https://s3-eu-west-1.amazonaws.com/dw-itops-public/int46elks/ivr.mp3';

		// Setup the number for choice 1
		var connect1 = new connect;
		connect1.init(number[1], callerid);

		// Setup the number for choice 2
		var connect2 = new connect;
		connect2.init(number[2], callerid);

		// Setup the number for choice 3
		var connect3 = new connect;
		connect3.init(number[3], callerid);

		// Setup the play for choice 1
		var play1 = new play;
		play1.init('https://s3-eu-west-1.amazonaws.com/dw-itops-public/int46elks/wait.mp3', null, connect1.data());

		// Setup the play for choice 2
		var play2 = new play;
		play2.init('https://s3-eu-west-1.amazonaws.com/dw-itops-public/int46elks/wait.mp3', null, connect2.data());

		// Setup the play for choice 3
		var play3 = new play;
		play3.init('https://s3-eu-west-1.amazonaws.com/dw-itops-public/int46elks/wait.mp3', null, connect3.data());

		// Settup the IVR-menu and add destinations
		var ivr1 = new ivr;
		ivr1.init(ivrfile);
		ivr1.addDestination(1, play1.data());
		ivr1.addDestination(2, play2.data());
		ivr1.addDestination(3, play3.data());

		cb(ivr1);
	}
}