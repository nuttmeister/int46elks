var provider = require('./46elks/provider.js');
var call = require('./46elks/call.js');
var connect = require('./46elks/connect.js');

module.exports = function (cFrom, cTo, cVoiceStart, cHangup, cb) {
	// Setup object for connnect
	var connect1 = new connect;
	connect1.init('+46737990204');

	// Setup object for call, leave voice_start empty to connect to the regular voice_start
	var call1 = new call;
	call1.init(cFrom, cTo, JSON.stringify(connect1.data()), cHangup);

	// Make the call
	provider.makeCall(call1, cb);
}