var provider = require('./46elks/provider.js');
var sms = require('./46elks/sms.js');

module.exports = function(cFrom, cTo, cMessage, cb) {
	// Create the SMS object - not always necessary, but looks nice
	var sms1 = new sms;
	sms1.init(cFrom, cTo, cMessage);

	// Do something with the SMS (store in DB, etc), don't return anything to callback (that will generate an reply)

	// To send an immediate reply to an incomming SMS use or send text/plain to callback
	cb(sms1.reply('Tack för smset!'));
}